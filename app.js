const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

require('dotenv').config();

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

const mongodbString = `mongodb://${process.env.MONGODB_USERNAME}:${process.env.MONGODB_PASSWORD}@cluster0-shard-00-00-ia47z.gcp.mongodb.net:27017,cluster0-shard-00-01-ia47z.gcp.mongodb.net:27017,cluster0-shard-00-02-ia47z.gcp.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true`;

mongoose.connect(
  mongodbString,
  {
    useNewUrlParser: true
  }
);

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

  if(req.method==='OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
    return res.status(200).json({});
  }

  next();
});

const UserRoutes = require('./api/routes/UserRoute');
const ChatRoutes = require('./api/routes/ChatRoute');
const MessageRoutes = require('./api/routes/MessageRoute');


app.use('/api/user/', UserRoutes);
app.use('/api/chat/', ChatRoutes);
app.use('/api/message/', MessageRoutes);

app.use((req,res,next) => {
  const error = new Error('Not found');
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
}); 

module.exports = app;