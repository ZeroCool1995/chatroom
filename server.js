const http = require('http');
const app = require('./app');

let port = process.env.PORT || 8080;
let server = http.createServer(app);

server.listen(port, () => {
  console.log(`Live @ http://localhost:${port}`);
});