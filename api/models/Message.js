const mongoose = require('mongoose');

const messageSchema = mongoose.Schema({
  _id:            mongoose.Schema.Types.ObjectId,
  chatId:         mongoose.Schema.Types.ObjectId,
  createdBy:      mongoose.Schema.Types.ObjectId,
  dateTime:       { type: Date, required: true },
  message:        { type: String, required: true }
});

module.exports = mongoose.model('Message', messageSchema);