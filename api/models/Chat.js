const mongoose = require('mongoose');

const chatSchema = mongoose.Schema({
  _id:            mongoose.Schema.Types.ObjectId,
  participants:   { type: [mongoose.Schema.Types.ObjectId], required: true },
  avatar:         { type: String },
  name:           { type: String }
});

module.exports = mongoose.model('Chat', chatSchema);