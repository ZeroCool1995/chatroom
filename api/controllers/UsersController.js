const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const token = require('jsonwebtoken');

const User = require('../models/User');

exports.signup = (req, res, next) => {

  User
    .find({
      username: req.body.username
    })
    .exec()
    .then(user => {
      if(user.length >= 1)
        return res.status(409).json({message: "Username already exists."});
      else {
        bcrypt.hash(req.body.password, process.env.SALT, (err, hash) => {
          if(err) {
            return res.status(500).json({error: err});
          }
          else {
      
            const user = new User({
              _id: new mongoose.Types.ObjectId(),
              username: req.body.username,
              password: hash,
              name: req.body.name,
              surname: req.body.surname,
              avatar: null
            });
      
            user.save()
              .then(result => {
                console.log(user);
                res.status(201).json({message: 'Your account is created.'});
              })
              .catch(error => {
                res.status(500).json({error: error});
              });
          }
        });
      } 
    })
    .catch();
};

exports.login = (req, res, next) => {

  User.find({
    username: req.body.username
  })
    .exec()
    .then(user => {
      if(user.length < 1)
        return res.status(401).json({
          message: 'Authentication failed'
        });
      
      bcrypt.compare(req.body.password, user[0].password, (error, result) => {
        if(error)
          return res.status(401).json({
            message: 'Authentication failed'
          });

        if(result) {
          const t = token.sign({
              _id: user[0]._id,
              username: user[0].username
            }, 
            process.env.TOKEN_KEY,
            {
              expiresIn: "2h"
          });
          return res.status(200).json({
            message: 'Authentication failed',
            token: t
          });
        }

        return res.status(401).json({
          message: 'Authentication failed'
        });

      });
    })
    .catch();
};

exports.getUserById = (req, res, next) => {

  const id = req.params.userId;

  User.find({_id: id})
    .exec()
    .then((doc) => {
      console.log(doc);
      if(doc)
        res.status(200).json({
          doc:doc
        });
      else
        res.status(404).json({
          message: 'User ID doesn\'t exist'
        });
    })
    .catch((error) => {
      console.log(error);
      res.status(500).json({
        error: error
      });
    });
};

exports.getUsers = (req, res, next) => {

  User.find()
    .exec()
    .then((docs) => {
      console.log(docs.length);
      if(docs.length > 0)
        res.status(200).json({
          docs: docs
        });
      else 
        res.status(404).json({
          message: 'Users don\'t exist!'
        });
    })
    .catch((error) => {
      console.log(error);
      res.status(500).json({
        error: error
      });
    });
};

exports.updateUser = (req, res, next) => {

  const id = req.params.userId;
  const updateOps = {};

  for (const ops of req.body)
    if(ops.propName == "password")
      updateOps[ops.propName] = bcrypt.hashSync(ops.value, 10);
    else
      updateOps[ops.propName] = ops.value;

  User.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(result => {
      console.log(result);
      res.status(200).json({result});
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.deleteUser = (req, res, next) => {
  User
    .remove({ _id: req.params.userId })
    .exec()
    .then(result => {
      res.status(200).json({
        message: 'User is deleted'
      })
    })
    .catch((error) => {
      console.log(error);
      res.status(500).json({
        error: error
      });
    });
};