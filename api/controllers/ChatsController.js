const mongoose = require('mongoose');

const User = require('../models/Chat');

/**
 * Route: POST /
*/
exports.createChat = (req, res, next) => {

  const chat = new Chat({
    _id: new mongoose.Types.ObjectId()
  });
  const userId = req.userData._id;
      
  chat
    .participants
    .push(userId);

  chat
    .save()
    .then(result => {
      res
        .status(201)
        .json({
          message: 'Your message is created.'
        });
    })
    .catch(error => {
      res
        .status(500)
        .json({
          error: error
        });
    });
};

/**
 * Route: PUT /:chatId/add-participants/
 */
exports.addParticipants = (res, req, next) => {

  const chatId = req.params.chatId;
  const usersId = req.body.usersId;

  Chat
    .update(
      { _id: chatId },
      { $push: { participants: { $each: usersId } }}  
    )
    .exec()
    .then(result => {
      res
        .status(200)
        .json({
          message: 'Participants added'
        });
    })
    .catch(error => {
      res
        .status(500)
        .json({
          error: error
        });
    });
};

/**
 * Route: GET /:chatId
 */

exports.getChatById = (req, res, next) => {

  const chatId = req.params.chatId;

  Chat
    .find({
      _id: chatId
    })
    .exec()
    .then(result => {
      if(result)
        res
          .status(200)
          .json({
            data: result
          });
      else
          res
            .status(404)
            .json({
              messsage: 'Chat ID doesn\'t exist'
            });
    })
    .catch(error => {
      res
        .status(500)
        .json({
          error: error
        });
    });
};

exports.getChatByUserId = (req, res, next) => {

  const userId = req.params.userId;

  Chat
    .find({
      participants: userId
    })
    .exec()
    .then(result => {
      if(result)
        res
          .status(200)
          .json({
            data: result
          });
      else
          res
            .status(404)
            .json({
              messsage: 'User has no chats'
            });
    })
    .catch(error => {
      res
        .status(500)
        .json({
          error: error
        });
    });
};



/**
 * Route: DELETE /:chatId
*/

exports.deleteChat = (req, res, next) => {

  const chatId = req.params.chatId;

  Chat
    .remove({
      _id: chatId
    })
    .exec()
    .then(result => {
      res 
        .status(200)
        .json({
          message: 'Chat is deleted'
        });
    })
    .catch(error => {
      res
        .status(500)
        .json({
          error: error
        });
    });
};