const mongoose = require('mongoose');

const Message = require('../models/Message');

/**
 * Route: POST /
*/
exports.createMessage = (req, res, next) => {

  const chatId = req.body.chatId;
  const createdBy = req.userData._id;
  const dateTime = new Date();
  const messageBody = req.body.message;

  const message = new Message({
    _id: mongoose.Types.ObjectId(),
    chatId: chatId,
    createdBy: createdBy,
    dateTime: dateTime,
    message: messageBody
  });

  message
    .save()
    .then(result => {
      res
        .status(201)
        .json({
          message: 'Your message is created.'
        });
    })
    .catch(error => {
      res
        .status(500)
        .json({
          error: error
        });
    });    
};

/**
 * Route: GET /:chatId
 */
exports.getMessages = (req, res, next) => {

  const chatId = req.params.chatId;

  Chat
    .find({
      chatId: chatId
    })
    .exec()
    .then(result => {
      if(result)
        res
          .status(200)
          .json({
            data: result
          });
      else
          res
            .status(404)
            .json({
              messsage: 'User has no chats'
            });
    })
    .catch(error => {
      res
        .status(500)
        .json({
          error: error
        });
    });
};