const express = require('express');
const router = express.Router();

const messageController = require('../controllers/MessagesController');
const auth = require('../middleware/CheckAuth');

router.post('/', auth, messageController.createMessage);
router.get('/:userId/user', auth, messageController.getMessages);

module.exports = router;