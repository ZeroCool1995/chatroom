const express = require('express');
const router = express.Router();

const chatController = require('../controllers/ChatsController');
const auth = require('../middleware/CheckAuth');

router.post('/', auth, chatController.createChat);
router.put('/:chatId/add-participants/', auth, chatController.addParticipants);
router.get('/:chatId/', auth, chatController.getChatById);
router.get('/:userId/user', auth, chatController.getChatByUserId);
router.delete('/:chatId/', auth, chatController.deleteChat);

module.exports = router;