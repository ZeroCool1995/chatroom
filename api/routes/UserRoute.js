const express = require('express');
const router = express.Router();

const userController = require('../controllers/UsersController');
const auth = require('../middleware/CheckAuth');

router.post('/signup', userController.signup);
router.post('/login', userController.login);
router.get('/:userId', userController.getUserById);
router.get('/', userController.getUsers);
router.put('/:userId', auth, userController.updateUser);
router.delete('/:userId', auth, userController.deleteUser);


module.exports = router;